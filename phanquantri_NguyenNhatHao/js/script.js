var data = []

function getTODO() {
    axios({
        method: 'GET',
        url: 'https://6350a8293e9fa1244e4a524d.mockapi.io/products?fbclid=IwAR0H2QfH8w9hz7wGltlNa0AtcQai0YmovwE-SXAl1Ocg4Je4R1xFuiFznXA',

    }).then(res => {
        data = res.data
        renderSP(res.data)
    }).catch(err => {
        console.log(err);
    });

}

function del(name) {

    for (let i = 0; i < data.length; i++) {
        if (data[i].name == name) {
            data.splice(i, 1);
            renderSP(data)
        }
    }

}

function getInfo() {
    var name = document.querySelector('#tensp').value;
    var price = document.querySelector('#price').value;
    var screen = document.querySelector('#screen').value;
    var backCamera = document.querySelector('#backCamera').value;
    var frontCamera = document.querySelector('#frontCamera').value;
    var img = document.querySelector('#img').value;
    var desc = document.querySelector('#desc').value;
    var type = document.querySelector('#type').value;


    return {
        name,
        price,
        screen,
        backCamera,
        frontCamera,
        img,
        desc,
        type
    };
}



function clearInfo() {
    document.querySelector('#tensp').value = ' ';
    document.querySelector('#price').value = ' ';
    document.querySelector('#screen').value = ' ';
    document.querySelector('#backCamera').value = ' ';
    document.querySelector('#frontCamera').value = ' ';
    document.querySelector('#img').value = ' ';
    document.querySelector('#desc').value = ' ';
    document.querySelector('#type').value = ' ';
}
var btnThemSP = document.querySelector('#btnThemSP');
var btnCapNhat = document.querySelector('#btnCapNhat');
btnThemSP.addEventListener('click', function() {
    var info = getInfo();


    console.log(info)

    data.push(info);

    clearInfo();
})



btnCapNhat.addEventListener('click', function() {
    renderSP(data);
})

function renderSP(dataProduct) {
    var content = '';
    for (var i = 0; i < dataProduct.length; i++) {
        var product = dataProduct[i];
        content += `
        <tr >
            <td >${product.name}</td>
            <td>${product.price}</td>

            <td>${product.screen}</td>
            
            <td>${product.backCamera}</td>
            <td>${product.frontCamera}</td>
            <td><img  style="width: 100px;" src=${product.img}></td>
             <td>${product.desc}</td>

            <td>${product.type}</td>
            

            <td>
                <button onclick="del('${product.name}')"   class="btn btn-danger btnDel ">Xóa</button>
                <button class="btn btn-info">Sửa</button>
            </td>
        </tr>
        `
    }
    document.querySelector('#tableDanhSach').innerHTML = content;

}