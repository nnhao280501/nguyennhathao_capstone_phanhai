const BASE_URL = "https://6350a8293e9fa1244e4a524d.mockapi.io";

let cart = [];

const cartItem = "cartItem";

let dataJSON = localStorage.getItem(cartItem);
if (dataJSON) {
  let dataRaw = JSON.parse(dataJSON);
  cart = dataRaw.map((item) => {
    return new CartProduct(item.product, item.quantity);
  });
}
let saveLocalStorage = () => {
  let dataJSON = JSON.stringify(cart);
  localStorage.setItem(cartItem, dataJSON);
};

let fetchListProducts = () => {
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then(function (item) {
      renderListProducts(item.data);
      console.log("item: ", item);
    })
    .catch(function (err) {
      console.log("err: ", err);
    });
};
fetchListProducts();
//filter
let filterProduct = () => {
  axios({
    url: `${BASE_URL}/products`,
    method: "GET",
  })
    .then((item) => {
      filterList(item.data);
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
//thêm sp vào giỏ
let addItem = (cartItem) => {
  let item = new PRODUCT(
    cartItem.id,
    cartItem.name,
    cartItem.price,
    cartItem.screen,
    cartItem.backCamera,
    cartItem.frontCamera,
    cartItem.img,
    cartItem.desc,
    cartItem.type
  );

  addProductToCart(item, cart);
  return cart;
};

let addProduct = (id) => {
  axios({
    url: `${BASE_URL}/products/${id}`,
    method: "GET",
  })
    .then((res) => {
      let result = addItem(res.data);
      renderCart(result);
      saveLocalStorage();
      calculation();
      swal("Sản Phẩm Đã Được Thêm Vào Giỏ Hàng");
    })
    .catch((err) => {
      console.log("err: ", err);
      swal("Thêm Thất Bại");
    });
};
//xóa
let clearCart = () => {
  swal("Đơn hàng đã được thanh toán", "Cám ơn quý khách", "success");
  cart = [];
  renderCart(cart);
  saveLocalStorage();
  calculation();
  document.getElementById("showcart").style.display = "none";
};
//đếm số item add
let calculation = () => {
  let cartIcon = document.getElementById("cartAmount");
  cartIcon.innerHTML = cart.map((x) => x.quantity).reduce((x, y) => x + y, 0);
};
calculation();
