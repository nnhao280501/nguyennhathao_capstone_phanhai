//render dssp

let renderListProducts = (list) => {
  let contentHTML = "";
  list.forEach(function (item) {
    contentHTML += `
<div class="card col-md-3">
  <div class="card-header">
    <div class="name">${item.name}</div>
  </div>
  <div class="card-body">
   <img src="${item.img}" alt="" />
  </div>
  <div class="card-content">
    <div class="screen">
  <span>Screen:${item.screen} </span>
 </div>
 <div class="backcam">
  <span>backCamera:${item.backCamera} </span>
 </div>
 <div class="frontcam">
  <span>frontCamera:${item.frontCamera} </span>
 </div>
 <div class="desc"> ${item.desc}</div>
 </div>
  <div class="card-footer">
  <div class="price"> Giá: $ ${item.price}</div>
  <button class=" btn btn-danger" onclick="addProduct(${item.id})">Add To Cart </button>
  </div>
</div>
    `;
  });
  document.getElementById("cardContainer").innerHTML = contentHTML;
};

//filter product
let filterList = (productList) => {
  let selectProduct = document.getElementById("productList");

  if (selectProduct.value === "All") {
    return renderListProducts(productList);
  } else {
    let product = productList.filter((item) => {
      return item.type === selectProduct.value;
    });
    renderListProducts(product);
  }
};

//show cart
document.getElementById("showcart").style.display = "none";
let showcart = () => {
  var isCartShow = document.getElementById("showcart");
  if (isCartShow.style.display == "block") {
    isCartShow.style.display = "none";
  } else {
    isCartShow.style.display = "block";
  }
  renderCart(cart);
};

//push product to cart[]
let addProductToCart = function (productCart, cart) {
  let quantity = 1;
  let product = new CartProduct(productCart, quantity);
  if (cart.length == 0) {
    cart.push(product);
  } else {
    for (let i = 0; i < cart.length; i++) {
      if (cart[i].product.id == productCart.id) {
        cart[i].quantity++;
        return;
      } else {
        let itemExist = cart.find((item) => {
          return item.product.id == productCart.id;
        });
        if (itemExist == undefined) {
          cart.push(product);
          return;
        }
      }
    }
  }
};

// render sp trong cart
let renderCart = (listCartProduct) => {
  let contentHTML = "";
  let total = 0;

  listCartProduct.forEach((item, index) => {
    total += itemTotalPrice(item.product.price, item.quantity);
    contentHTML += `
      
    <tr>
    <th scope="row" class="item-cart  justify-content-between ">
      <p >
        <img src="${item.product.img}" alt=""></p>
        <p >${item.product.name}</p>
    </td>
    <td>
        <span onclick="minusNumber(${index})"> <i class="fa fa-minus-circle"></i></span>
       
        <span class="col">${item.quantity}</span>
        
        <span onclick="plusNumber(${index})"><i class="fa fa-plus-circle"></i></span>
        
    </td>
        
    <td>
        <p class="col p-0">$ ${itemTotalPrice(
          item.product.price,
          item.quantity
        )}</p>
    </td>
    <td>
        <span onclick="deleteProduct(${index})">
        <i class="fa fa-trash"></i></span>
    </td> 
    </tr>
        `;
  });

  document.getElementById("cart").innerHTML = contentHTML;
  document.getElementById("priceTotal").innerHTML = `TỔNG TIỀN: $ ${total}`;
};

//cộng số lượng
let plusNumber = (qty) => {
  cart[qty].quantity++;
  renderCart(cart);
  saveLocalStorage();
  calculation();
};

//trừ số lượng
let minusNumber = (qty) => {
  if (cart[qty].quantity == 1) {
    cart.splice(qty, 1);
    renderCart(cart);
    saveLocalStorage();
    calculation();
  } else {
    cart[qty].quantity--;
    renderCart(cart);
    saveLocalStorage();
    calculation();
  }
};
let itemTotalPrice = (price, quantity) => {
  return price * quantity;
};

//xóa sp
let deleteProduct = (index) => {
  cart.splice(index, 1);
  renderCart(cart);
  saveLocalStorage();
  calculation();
};
